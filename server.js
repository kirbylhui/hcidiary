
//All Required Files : 

      var express = require('express');
      // Create the app
      var app = express();
      // File System for loading the list of words
      var fs = require('fs');
      // Cors for allowing "cross origin resources"
      var cors = require('cors');
      app.use(cors());
      // A new package for sentiment analysis
      var sentiment = require('sentiment');


//HOST / POST Handling :

      // "body parser" is need to deal with post requests
      var bodyParser = require('body-parser');
      app.use(bodyParser.json()); // support json encoded bodies
      app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
      // This is for hosting files
      app.use(express.static('public'));


// Set Up Server :

      // process
      var server = app.listen(process.env.PORT || 8000, listen);
      // This call back just tells us that the server has started
      function listen() {
        var host = server.address().address;
        var port = server.address().port;
        console.log('Example app listening at http://' + host + ':' + port);
      }


// POST analysis function :

      // This is to analyze a larger body of text
      app.post('/analyze', analyze);
      function analyze(req, res) {
        // The bodyParse package allows us to easily just grab the "text" field
        var text = req.body.text;
        // Send back the results of the analysis
        // Use the additional words too
        var reply = sentiment(text);
        res.send(reply);
}
